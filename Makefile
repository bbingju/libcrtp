include config.mk

LIB_STATIC = libcrtp.a

SRCS = \
	src/internal/cbuffer.c \
	src/crtp.c \
	src/syslink.c \
	src/syslink_uart.c \
	src/commander.c \
	src/param.c \
	src/shift_event.c

OBJS = $(patsubst %.c,%.o,$(SRCS))

CFLAGS += -I./include -I./src/internal 

.PHONY: clean examples

all: $(LIB_STATIC) examples

$(LIB_STATIC): $(OBJS)
	$(AR) rcs $@ $^

examples:
	@make -C $@

clean:
	$(RM) $(LIB_STATIC) $(OBJS)
	@make -C examples clean
