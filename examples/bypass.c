#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h>
#include "syslink.h"

#define RADIO_DEVNAME         "/dev/ttyS1"
#define RADIO_BAUDRATE        115200
#define FC_DEVNAME            "/dev/ttyS2"
#define FC_BAUDRATE           380400

#define TIMEOUT_MS            10

int main ()
{
    syslink_t *fc = syslink_new(FC_DEVNAME, FC_BAUDRATE);
    syslink_t *radio = syslink_new(RADIO_DEVNAME, RADIO_BAUDRATE);

    struct pollfd fds[4];
    int ret;

    if (!fc || !radio) {
        perror("syslink_new");
        exit(EXIT_FAILURE);
    }

    fds[0].fd = syslink_fd(fc);
    fds[0].events = POLLIN /* | POLLOUT */;
    fds[1].fd = syslink_fd(radio);
    fds[1].events = POLLIN /* | POLLOUT */;

    while (1) {
        ret = poll(fds, 1, TIMEOUT_MS);
        if (ret == -1) {
            perror("poll");
            exit(EXIT_FAILURE);
        }

        if (fds[0].revents & POLLIN) {
            syslink_packet_t pkt = { 0 };
            if (syslink_receive(fc, &pkt)) {
                /* if (pkt.type == SYSLINK_RADIO_RAW) */
                syslink_dump(&pkt, "recv form fc");
                syslink_send(radio, pkt.type, pkt.data, pkt.length);
            }
        }

        if (fds[1].revents & POLLIN) {
            syslink_packet_t pkt = { 0 };
            if (syslink_receive(radio, &pkt)) {
                if (pkt.type == SYSLINK_RADIO_RAW)
                    syslink_dump(&pkt, "recv from radio");
                syslink_send(fc, pkt.type, pkt.data, pkt.length);
            }
         }
    }

    syslink_del(radio);
    syslink_del(fc);

    exit(EXIT_SUCCESS);
}
