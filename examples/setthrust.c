#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "syslink.h"
#include "syslink_uart.h"
#include "commander.h"

#define FC_DEVNAME            "/dev/ttyS2"
#define FC_BAUDRATE           380400

static void _help(int argc, char *argv[])
{
    printf("%s <thrust> <num_of_times>\n", argv[0]);
}

int main (int argc, char *argv[])
{
    if (argc < 2) {
        _help(argc, argv);
        return 0;
    }

    int thrust = atoi(argv[1]);
    int num_of_times = (argc == 2) ? 1 : atoi(argv[2]);
    
    syslink_t *fc = syslink_new(FC_DEVNAME, FC_BAUDRATE);
    if (!fc)
        return -1;

    commander_init();

    for (int i = 0; i < num_of_times; ++i) {
        commander_manual(fc, 0.0, 0.0, 0, thrust);
        usleep(100000);
    }

    commander_deinit();

    syslink_del(fc);

    return 0;
}
