#include <stdio.h>
#include <unistd.h>
#include "syslink.h"
#include "syslink_uart.h"
#include "commander.h"

#define FC_DEVNAME            "/dev/ttyS2"
#define FC_BAUDRATE           380400

int main (int argc, char *argv[])
{
    syslink_t *fc = syslink_new(FC_DEVNAME, FC_BAUDRATE);
    if (!fc)
        return -1;

    commander_init();

    commander_hl_takeoff(fc, 0, 0.4f, 10.0f);
    sleep(10);
    commander_hl_stop(fc, 0);

    commander_deinit();

    syslink_del(fc);

    return 0;
}
