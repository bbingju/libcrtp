#include <stdio.h>
#include <unistd.h>
#include "syslink.h"
#include "syslink_uart.h"
#include "commander.h"

#define FC_DEVNAME            "/dev/ttyS2"
#define FC_BAUDRATE           380400

int main (int argc, char *argv[])
{
    syslink_t *fc = syslink_new(FC_DEVNAME, FC_BAUDRATE);
    if (!fc)
        return -1;

    commander_init();

    /* for (int i = 0; i < 100; ++i) { */
    /*     commander_manual(fc, 34.3, 34.9, 0, 40000); */
    /*     usleep(100000); */
    /* } */

    /* shift_event_command(CRTP_PORT_TO_AP, SEVT_CMD_CONNECT); */
    /* shift_event_result(CRTP_PORT_TO_SC, SEVT_RET_OK); */

    commander_hover(fc, 0, 0, 0, 0.4);
    /* for (int i = 0; i < 100; ++i) { */
    /*     crtp_setpoint_hover(fc, 34.34, 35, 100, 0.4); */
    /*     crtp_setpoint_hover(fc, 0, 0, 0, 0.4); */
    /*     usleep(100000); */
    /* } */

    commander_deinit();

    syslink_del(fc);

    return 0;
}
