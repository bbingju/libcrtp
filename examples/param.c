#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "syslink.h"
#include "param.h"

#define FC_DEVNAME            "/dev/ttyS2"
#define FC_BAUDRATE           380400

int main ()
{
    syslink_t *fc = syslink_new(FC_DEVNAME, FC_BAUDRATE);
    if (!fc) {
        perror("syslink_new");
        return -1;
    }

    param_init();

    param_toc_getinfo(fc);

    param_deinit();

    syslink_del(fc);

    return 0;
}
