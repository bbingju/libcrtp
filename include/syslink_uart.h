#ifndef SYSLINK_UART_H
#define SYSLINK_UART_H

#ifdef __cplusplus
extern "C" {
#endif

int syslink_uart_open(const char *port, int baudrate);
int syslink_uart_close(int fd);
int syslink_uart_read(int fd, unsigned char *buffer, unsigned int buffer_size);
int syslink_uart_write(int fd, unsigned char *buffer, unsigned int buffer_size);

#ifdef __cplusplus
}
#endif

#endif  /* SYSLINK_UART_H */
