/**
 * Copyright (c) 2018, This is engineering Inc.
 *
 * All rights reserved.
 *
 * shift_event.h - ShiFT global event definition.
 *
 */
#ifndef SHIFT_EVENT_H
#define SHIFT_EVENT_H

#include <stdbool.h>
#include "syslink.h"
#include "shift_event/shift_event.h"

#ifdef __cplusplus
extern "C" {
#endif

void shift_event_init();
void shift_event_deinit();
void shift_event_command(syslink_t *sl, unsigned int desPort, shift_event_t type);
void shift_event_result(syslink_t *sl, unsigned int desPort, shift_event_t type);

#ifdef __cplusplus
}
#endif

#endif /* SHIFT_EVENT_H */
