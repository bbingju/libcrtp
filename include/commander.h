#ifndef COMMANDER_H
#define COMMANDER_H

#include <stdint.h>
#include "syslink.h"

#ifdef __cplusplus
extern "C" {
#endif

void commander_init();
void commander_deinit();
void commander_manual(syslink_t *sl, float roll, float pitch, float yaw, uint16_t thrust);
void commander_velocity(syslink_t *sl, float vx, float vy, float vz, float yawrate);
void commander_zdistance(syslink_t *sl, float roll, float pitch, float yawrate, float zdistance);
void commander_althold(syslink_t *sl, float roll, float pitch, float yawrate, float zvelocity);
void commander_hover(syslink_t *sl, float vx, float vy, float yawrate, float zdistance);

void commander_hl_takeoff(syslink_t *sl, uint8_t groupmask, float height, float duration);
void commander_hl_land(syslink_t *sl, uint8_t groupmask, float height, float duration);
void commander_hl_stop(syslink_t *sl, uint8_t groupmask);

#ifdef __cplusplus
}
#endif

#endif  /* COMMANDER_H */
