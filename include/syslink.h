#ifndef SYSLINK_H
#define SYSLINK_H

#include <stdint.h>

#define SYSLINK_MTU 32

#define CRTP_START_BYTE  0xAA
#define SYSLINK_START_BYTE1 0xBC
#define SYSLINK_START_BYTE2 0xCF

// Defined packet types
#define SYSLINK_GROUP_MASK    0xF0

#define SYSLINK_RADIO_GROUP         0x00
#define SYSLINK_RADIO_RAW           0x00
#define SYSLINK_RADIO_CHANNEL       0x01
#define SYSLINK_RADIO_DATARATE      0x02
#define SYSLINK_RADIO_CONTWAVE      0x03
#define SYSLINK_RADIO_RSSI          0x04
#define SYSLINK_RADIO_ADDRESS       0x05
#define SYSLINK_RADIO_RAW_BROADCAST 0x06
#define SYSLINK_RADIO_POWER         0x07

#define SYSLINK_PM_GROUP              0x10
#define SYSLINK_PM_SOURCE             0x10
#define SYSLINK_PM_ONOFF_SWITCHOFF    0x11
#define SYSLINK_PM_BATTERY_VOLTAGE    0x12
#define SYSLINK_PM_BATTERY_STATE      0x13
#define SYSLINK_PM_BATTERY_AUTOUPDATE 0x14

#define SYSLINK_OW_GROUP    0x20
#define SYSLINK_OW_SCAN     0x20
#define SYSLINK_OW_GETINFO  0x21
#define SYSLINK_OW_READ     0x22
#define SYSLINK_OW_WRITE    0x23

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _syslink_packet
{
  uint8_t type;
  uint8_t length;
  uint8_t data[SYSLINK_MTU];
} __attribute__((packed)) syslink_packet_t;

/* struct _syslink { */
/*     int fd; */
/*     //pthread_mutex_t mutex; */

/*     int state; */
/*     struct _syslink_packet pkt; */
/*     uint8_t crc[2]; */
/*     int data_index; */

/*     struct _cbuffer cbuf; */
/*     uint8_t cbuf_raw[128]; */
/* }; */

struct _syslink;
typedef struct _syslink syslink_t;

syslink_t * syslink_new      (const char *port, int baudrate);
void        syslink_del      (syslink_t *sl);
int         syslink_send     (syslink_t *sl, uint8_t type, void *data, uint8_t data_size);
int         syslink_receive  (syslink_t *sl, syslink_packet_t *slp);
int         syslink_fd       (syslink_t *sl);
void        syslink_dump     (syslink_packet_t *slp, const char *str);
const char *syslink_type_name(uint8_t type);

#ifdef __cplusplus
}
#endif

#endif  /* SYSLINK_H */
