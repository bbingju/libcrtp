#ifndef CRTP_H
#define CRTP_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	CRTP_PORT_CONSOLE          = 0,
	CRTP_PORT_PARAM            = 2,
	CRTP_PORT_SETPOINT         = 3,
	CRTP_PORT_MEM              = 4,
	CRTP_PORT_LOG              = 5,
	CRTP_PORT_LOCALIZATION     = 6,
	CRTP_PORT_SETPOINT_GENERIC = 7,
	CRTP_PORT_SETPOINT_HL      = 8,
	//Use to Destination(9~12)
	CRTP_PORT_TO_FC			   = 9,
	CRTP_PORT_TO_AP			   = 10,
	CRTP_PORT_TO_RF 		   = 11,
	CRTP_PORT_TO_SC			   = 12,

	CRTP_PORT_PLATFORM         = 13,
	CRTP_PORT_DEBUG            = 14,
	CRTP_PORT_LINK             = 15,
} crtp_port_t;

#define CRTP_MAX_DATA_SIZE 30

typedef enum {
	CRTP_RET_FAILED  = -1,
	CRTP_RET_SUCCESS = 0,
} crtp_ret_t;

typedef enum {
    CRTP_DRIVER_TYPE_SYSLINK,
} crtp_driver_type_e;

struct _crtp_driver {
    crtp_driver_type_e type;
};

typedef unsigned char crtp_header_t;
/* typedef unsigned char crtp_port_t; */
typedef unsigned char crtp_channel_t;

struct _crtp {
    crtp_port_t port;
    crtp_channel_t channel;
    unsigned char data[CRTP_MAX_DATA_SIZE];
    int data_size;

    struct _crtp_driver driver;

    void (*make_data)(struct _crtp *self, void *arg);
};

typedef struct _crtp crtp_t;

crtp_ret_t crtp_init(crtp_t *c);
crtp_ret_t crtp_deinit(crtp_t *c);

crtp_ret_t crtp_set_header(crtp_t *c, crtp_port_t port, crtp_channel_t channel);
crtp_ret_t crtp_pack(crtp_t *c, void *arg, void *outbuf, int *size);
crtp_ret_t crtp_unpack(void *inbuf, crtp_t *c);

#ifdef __cplusplus
}
#endif

#endif  /* CRTP_H */
