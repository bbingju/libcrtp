#ifndef PARAM_H
#define PARAM_H

#include <stdint.h>
#include "syslink.h"

#ifdef __cplusplus
extern "C" {
#endif

void param_init();
void param_deinit();
int param_toc_getinfo(syslink_t *sl);
void param_request_by_id(int id, void *response);
void param_request(const char *name, void *response);

#ifdef __cplusplus
}
#endif

#endif  /* PARAM_H */
