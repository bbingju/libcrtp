CROSS_COMPILE = arm-linux-gnueabi-
CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar

CFLAGS = -std=c99 -Wall

%.o: %c
	$(CC) $(CFLAGS) -c $<

