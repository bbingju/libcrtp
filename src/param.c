#include "crtp.h"
#include "syslink.h"
#include "param.h"

enum {
    PARAM_CHANNEL_TOC = 0,
    PARAM_CHANNEL_READ,
    PARAM_CHANNEL_WRITE,
    PARAM_CHANNEL_MISC,
};

enum {
    PARAM_CMD_GETITEM = 0,
    PARAM_CMD_GETINFO,
    PARAM_CMD_GETITEM_V2,
    PARAM_CMD_GETINFO_V2,
};

struct _param_arg {
    uint8_t channel;
    uint8_t cmd;
};

static  void _make_data(struct _crtp *self, void *arg);

static struct _crtp _crtp_param = {
    .port = CRTP_PORT_PARAM,
    .make_data = _make_data,
};

static  void _make_data(struct _crtp *self, void *arg)
{
    struct _param_arg *pa = arg;

    if (pa->channel == PARAM_CHANNEL_TOC) {
        self->data_size = 1;
        self->data[0] = pa->cmd;
    }
}

void param_init()
{
    crtp_init(&_crtp_param);
}

void param_deinit()
{
    crtp_deinit(&_crtp_param);
}

int param_toc_getinfo(syslink_t *sl)
{
    uint8_t buffer[12] = { 0 };
    int buffer_size = 0;
    struct _param_arg arg = {
        .channel = PARAM_CHANNEL_TOC,
        .cmd = PARAM_CMD_GETINFO,
    };

    crtp_set_header(&_crtp_param, _crtp_param.port, PARAM_CHANNEL_TOC);
    crtp_pack(&_crtp_param, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);

    syslink_packet_t pkt;
    int ret = syslink_receive(sl, &pkt);
    if (ret) {
        syslink_dump(&pkt, "param_toc_getinfo");
    }
    return ret;

}

void param_request_by_id(int id, void *response)
{
}

void param_request(const char *name, void *response)
{
}

