#include <stdint.h>
#include <string.h>

#include "crtp.h"
#include "syslink.h"
#include "commander.h"

typedef enum {
    COMMANDER_TYPE_MANUAL,
    COMMANDER_TYPE_GENERIC,
    COMMANDER_TYPE_HL,
} commander_type_e;

enum {
    COMMANDER_GENERIC_TYPE_STOP,
    COMMANDER_GENERIC_TYPE_VELOCITYWORLD,
    COMMANDER_GENERIC_TYPE_ZDISTANCE,
    COMMANDER_GENERIC_TYPE_CPPMEMU,
    COMMANDER_GENERIC_TYPE_ALTHOLD,
    COMMANDER_GENERIC_TYPE_HOVER,
    COMMANDER_GENERIC_TYPE_FULLSTATE,
    COMMANDER_GENERIC_TYPE_POSITION,
};

typedef uint8_t commander_generic_type_e;

enum {
    COMMANDER_HL_SET_GROUP_MASK,
    COMMANDER_HL_TAKEOFF,
    COMMANDER_HL_LAND,
    COMMANDER_HL_STOP,
    COMMANDER_HL_GO_TO,
    COMMANDER_HL_START_TRAJECTORY,
    COMMANDER_HL_DEFINE_TRAJECTORY,
};
typedef uint8_t commander_hl_type_e;

struct _commander_manual {
    float roll;
    float pitch;
    float yaw;
    uint16_t thrust;
} __attribute__((packed));

struct _commander_generic_velocity {
    float vx;
    float vy;
    float vz;
    float yawrate;
} __attribute__((packed));

struct _commander_generic_zdistance {
    float roll;
    float pitch;
    float yawrate;
    float zdistance;
} __attribute__((packed));

struct _commander_generic_althold {
    float roll;
    float pitch;
    float yawrate;
    float zvelocity;
} __attribute__((packed));

struct _commander_generic_hover {
    float vx;
    float vy;
    float yawrate;
    float zdistance;
} __attribute__((packed));

struct _commander_generic {
    commander_generic_type_e type;
    union {
        struct _commander_generic_velocity velocity;
        struct _commander_generic_zdistance zdistance;
        struct _commander_generic_althold althold;
        struct _commander_generic_hover hover;
    };
} __attribute__((packed));

struct _commander_hl_data_set_group_mask {
    uint8_t groupmask;
} __attribute__((packed));

struct _commander_hl_data_takeoff {
    uint8_t groupmask;
    float height;               /* m (absolute) */
    float duration;             /* s (time it should take until target height is reached) */
} __attribute__((packed));

struct _commander_hl_data_land {
    uint8_t groupmask;
    float height;               /* m (absolute) */
    float duration;
} __attribute__((packed));

struct _commander_hl_data_stop {
    uint8_t groupmask;
} __attribute__((packed));

struct _commander_hl_data_go_to {
    uint8_t groupmask;
    uint8_t relative;           /* set to true, if position/yaw are relative to current setpoint */
    float x;                    /* m */
    float y;                    /* m */
    float z;                    /* m */
    float yaw;                  /* deg */
    float duration;             /* sec */
} __attribute__((packed));

struct _commander_hl_data_start_trajectory {
    uint8_t groupmask;
    uint8_t relative;           /* set to true, if trajectory should shifted to current setpoint */
    uint8_t reversed;           /* set to true, if trajectory should be executed in reverse */
    uint8_t trajectory_id;      /* id of the trajectory (previously defined by COMMAND_DEFINE_TRAJECTORY) */
    float timescale;            /* time factor; 1 = original speed; >1: slower; <1: faster */
} __attribute__((packed));

struct _trajectory_desc {
    uint8_t trajectory_location; /* one of trajectory_location_e */
    uint8_t trajectory_type;     /* one of trajectory_type_e */
    union {
        struct {
            uint32_t offset;    /* offset in uploaded memory */
            uint8_t n_pieces;
        } __attribute__((packed)) mem;
    } trajectory_identifier;
} __attribute__((packed));

struct _commander_hl_data_define_trajectory {
    uint8_t trajectory_id;      /* id of the trajectory (previously defined by COMMAND_DEFINE_TRAJECTORY) */
    struct _trajectory_desc description;
} __attribute__((packed));

struct _commander_hl {
    commander_hl_type_e type;
    union {
        struct _commander_hl_data_set_group_mask  set_group_mask;
        struct _commander_hl_data_takeoff takeoff;
        struct _commander_hl_data_land land;
        struct _commander_hl_data_stop stop;
        struct _commander_hl_data_go_to go_to;
        struct _commander_hl_data_start_trajectory start_trajectory;
        struct _commander_hl_data_define_trajectory define_trajectory;
    };
} __attribute__((packed));

struct _commander_arg {
    commander_type_e type;

    union {
        struct _commander_manual manual;
        struct _commander_generic generic;
        struct _commander_hl hl;
    };
};

static  void _make_data(struct _crtp *self, void *arg);

static struct _crtp crtp_commander = {
    .make_data = _make_data,
};

static  void _make_data(struct _crtp *self, void *arg)
{
    struct _commander_arg *ca = arg;

    if (ca->type == COMMANDER_TYPE_MANUAL) {
        self->data_size = sizeof(struct _commander_manual);
        memcpy(self->data, &ca->manual, self->data_size);
    }
    else if (ca->type == COMMANDER_TYPE_GENERIC) {
        struct _commander_generic *cg = &ca->generic;

        self->data[0] = cg->type;

        switch (cg->type) {
        case COMMANDER_GENERIC_TYPE_STOP:
            break;
        case COMMANDER_GENERIC_TYPE_VELOCITYWORLD:
            self->data_size = sizeof(struct _commander_generic_velocity) + 1;
            memcpy(&self->data[1], &cg->velocity, self->data_size - 1);
            break;
            break;
        case COMMANDER_GENERIC_TYPE_ZDISTANCE:
            self->data_size = sizeof(struct _commander_generic_zdistance) + 1;
            memcpy(&self->data[1], &cg->zdistance, self->data_size - 1);
            break;
        case COMMANDER_GENERIC_TYPE_CPPMEMU:
            break;
        case COMMANDER_GENERIC_TYPE_ALTHOLD:
            self->data_size = sizeof(struct _commander_generic_althold) + 1;
            memcpy(&self->data[1], &cg->althold, self->data_size - 1);
            break;
        case COMMANDER_GENERIC_TYPE_HOVER:
            self->data_size = sizeof(struct _commander_generic_hover) + 1;
            memcpy(&self->data[1], &cg->hover, self->data_size - 1);
            break;
        case COMMANDER_GENERIC_TYPE_FULLSTATE:
            break;
        case COMMANDER_GENERIC_TYPE_POSITION:
            break;
        default:
            break;
        }
    }
    else if (ca->type == COMMANDER_TYPE_HL) {
        struct _commander_hl *ch = &ca->hl;

        self->data[0] = ch->type;

        switch (ch->type) {
        case COMMANDER_HL_SET_GROUP_MASK:
            self->data_size = sizeof(struct _commander_hl_data_set_group_mask) + 1;
            memcpy(&self->data[1], &ch->set_group_mask, self->data_size - 1);
            break;
        case COMMANDER_HL_TAKEOFF:
            self->data_size = sizeof(struct _commander_hl_data_takeoff) + 1;
            memcpy(&self->data[1], &ch->takeoff, self->data_size - 1);
            break;
        case COMMANDER_HL_LAND:
            self->data_size = sizeof(struct _commander_hl_data_land) + 1;
            memcpy(&self->data[1], &ch->land, self->data_size - 1);
            break;
        case COMMANDER_HL_STOP:
            self->data_size = sizeof(struct _commander_hl_data_stop) + 1;
            memcpy(&self->data[1], &ch->stop, self->data_size - 1);
            break;
        case COMMANDER_HL_GO_TO:
            self->data_size = sizeof(struct _commander_hl_data_go_to) + 1;
            memcpy(&self->data[1], &ch->go_to, self->data_size - 1);
            break;
        case COMMANDER_HL_START_TRAJECTORY:
            self->data_size = sizeof(struct _commander_hl_data_start_trajectory) + 1;
            memcpy(&self->data[1], &ch->start_trajectory, self->data_size - 1);
            break;
        case COMMANDER_HL_DEFINE_TRAJECTORY:
            self->data_size = sizeof(struct _commander_hl_data_define_trajectory) + 1;
            memcpy(&self->data[1], &ch->define_trajectory, self->data_size - 1);
            break;
        default:
            break;
        }
    }
}

void commander_init()
{
    crtp_init(&crtp_commander);
}

void commander_deinit()
{
    crtp_deinit(&crtp_commander);
}

void commander_manual(syslink_t *sl, float roll, float pitch, float yaw, uint16_t thrust)
{
    uint8_t buffer[24] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_MANUAL,
        .manual = {
            .roll = roll,
            .pitch = pitch,
            .yaw = yaw,
            .thrust = thrust,
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_velocity(syslink_t *sl, float vx, float vy, float vz, float yawrate)
{
    uint8_t buffer[24] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_GENERIC,
        .generic = {
            .type = COMMANDER_GENERIC_TYPE_VELOCITYWORLD,
            .velocity = {
                .vx = vx,
                .vy = vy,
                .vz = vz,
                .yawrate = yawrate,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_zdistance(syslink_t *sl, float roll, float pitch, float yawrate, float zdistance)
{
    uint8_t buffer[24] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_GENERIC,
        .generic = {
            .type = COMMANDER_GENERIC_TYPE_ZDISTANCE,
            .zdistance = {
                .roll = roll,
                .pitch = pitch,
                .yawrate = yawrate,
                .zdistance = zdistance,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}


void commander_althold(syslink_t *sl, float roll, float pitch, float yawrate, float zvelocity)
{
    uint8_t buffer[24] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_GENERIC,
        .generic = {
            .type = COMMANDER_GENERIC_TYPE_ALTHOLD,
            .althold = {
                .roll = roll,
                .pitch = pitch,
                .yawrate = yawrate,
                .zvelocity = zvelocity,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_hover(syslink_t *sl, float vx, float vy, float yawrate, float zdistance)
{
    uint8_t buffer[24] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_GENERIC,
        .generic = {
            .type = COMMANDER_GENERIC_TYPE_HOVER,
            .hover = {
                .vx = vx,
                .vy = vy,
                .yawrate = yawrate,
                .zdistance = zdistance,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_hl_takeoff(syslink_t *sl, uint8_t groupmask, float height, float duration)
{
    uint8_t buffer[16] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_HL,
        .hl = {
            .type = COMMANDER_HL_TAKEOFF,
            .takeoff = {
                .groupmask = groupmask,
                .height = height,
                .duration = duration,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_HL, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_hl_land(syslink_t *sl, uint8_t groupmask, float height, float duration)
{
    uint8_t buffer[16] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_HL,
        .hl = {
            .type = COMMANDER_HL_LAND,
            .land = {
                .groupmask = groupmask,
                .height = height,
                .duration = duration,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_HL, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void commander_hl_stop(syslink_t *sl, uint8_t groupmask)
{
    uint8_t buffer[16] = { 0 };
    int buffer_size = 0;
    struct _commander_arg arg = {
        .type = COMMANDER_TYPE_HL,
        .hl = {
            .type = COMMANDER_HL_STOP,
            .stop = {
                .groupmask = groupmask,
            }
        }
    };

    crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_HL, 0);
    crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);

    syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}
