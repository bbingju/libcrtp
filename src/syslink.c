#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "syslink.h"
#include "syslink_uart.h"
#include "cbuffer.h"
#include "str.h"

typedef enum
{
    WAIT_FOR_1ST_START,
    WAIT_FOR_2ND_START,
    WAIT_FOR_TYPE,
    WAIT_FOR_LENGTH,
    WAIT_FOR_DATA,
    WAIT_FOR_CHECKSUM_1,
    WAIT_FOR_CHECKSUM_2
} syslink_rx_state_t;


struct _syslink {
    int fd;
    //pthread_mutex_t mutex;

    int state;
    struct _syslink_packet pkt;
    uint8_t crc[2];
    int data_index;

    struct _cbuffer cbuf;
    uint8_t cbuf_raw[128];
};

#define _parse_reset(s) do {                      \
        s->data_index = 0;                         \
        s->crc[0] = 0;                             \
        s->crc[1] = 0;                             \
        s->state = WAIT_FOR_1ST_START;             \
    } while (0);

static struct _syslink_packet * _parse(struct _syslink *this, uint8_t c)
{

    switch (this->state) {
    case WAIT_FOR_1ST_START:
        if (c == SYSLINK_START_BYTE1)
            this->state = WAIT_FOR_2ND_START;
        break;
    case WAIT_FOR_2ND_START:
        this->state = (c == SYSLINK_START_BYTE2) ? WAIT_FOR_TYPE : WAIT_FOR_1ST_START;
        break;
    case WAIT_FOR_TYPE:
        this->pkt.type = c;
        this->crc[0] = c;
        this->crc[1] = c;
        this->state = WAIT_FOR_LENGTH;
        break;
    case WAIT_FOR_LENGTH:
        if (c > SYSLINK_MTU) {
            _parse_reset(this);
        }
        else {
            this->pkt.length = c;
            this->crc[0] += c;
            this->crc[1] += this->crc[0];
            this->data_index = 0;
            this->state = (c > 0) ? WAIT_FOR_DATA : WAIT_FOR_CHECKSUM_1;
        }
        break;
    case WAIT_FOR_DATA:
        this->pkt.data[this->data_index++] = c;
		this->crc[0] += c;
		this->crc[1] += this->crc[0]; 
        if (this->data_index == this->pkt.length)
            this->state = WAIT_FOR_CHECKSUM_1;
        break;
    case WAIT_FOR_CHECKSUM_1:
        if (this->crc[0] == c) {
            this->state = WAIT_FOR_CHECKSUM_2;
        }
        else {
            /* fprintf(stderr, "Checksum error\n"); */
            _parse_reset(this);
        }
        break;
    case WAIT_FOR_CHECKSUM_2:
        if (this->crc[1] == c) {
            _parse_reset(this);
            return &this->pkt;
        }
        else {
            /* fprintf(stderr, "Checksum error\n"); */
            _parse_reset(this);
        }
        break;
    default:
        break;
    }

    return NULL;
}

static int syslink_init(struct _syslink *sl, const char *port, int baudrate)
{
    int fd = syslink_uart_open(port, baudrate);
    if (fd < 0) {
        fprintf(stderr, "syslink_uart_open error\n");
        return -1;
    }

    _parse_reset(sl);

    sl->cbuf.buffer = sl->cbuf_raw;
    sl->cbuf.size = 128;
    cbuffer_reset(&sl->cbuf);

    sl->fd = fd;

    return fd;
}

static void syslink_deinit(struct _syslink *sl)
{
    if (sl) {
        if (syslink_uart_close(sl->fd) < 0) {
            fprintf(stderr, "syslink_uart_close error\n");
            return;
        }
        _parse_reset(sl);
        cbuffer_reset(&sl->cbuf);
    }
}

syslink_t * syslink_new(const char *port, int baudrate)
{
    syslink_t *this = calloc(1, sizeof(syslink_t));
    if (!this)
        return NULL;

    if (syslink_init(this, port, baudrate) == -1) {
        free(this);
        return NULL;
    }

    return this;
}

void syslink_del(syslink_t *sl)
{
    if (sl) {
        syslink_deinit(sl);
    }
}

int syslink_send(struct _syslink *sl, uint8_t type, void *data, uint8_t data_size)
{
    int total_size;
    uint8_t cksum[2] = {0};
    uint8_t buffer[64] = {0};

    if (data_size > SYSLINK_MTU)
        return -1;

    buffer[0] = SYSLINK_START_BYTE1;
    buffer[1] = SYSLINK_START_BYTE2;
    buffer[2] = type;
    buffer[3] = data_size;

    memcpy(&buffer[4], data, data_size);
    total_size = data_size + 6;

    /* Calculate checksum delux */
    for (int i = 2; i < total_size - 2; ++i) {
        cksum[0] += buffer[i];
        cksum[1] += cksum[0];
    }
    buffer[total_size - 2] = cksum[0];
    buffer[total_size - 1] = cksum[1];

    /* for test */
    /* static struct _syslink_packet *pkt = NULL; */
    /* for (int i = 0; i < total_size; ++i) { */
    /*     pkt = _parse(buffer[i]); */
    /*     if (pkt) { */
    /*         printf("syslink %s (%d)\n", */
    /*                syslink_type_name(pkt->type), pkt->length); */
    /*         for (int j =0; j < pkt->length; ++j) */
    /*             printf("%02x ", pkt->data[j]); */
    /*         putchar('\n'); */
    /*     } */
    /* } */

    //pthread_mutex_lock(&mutex);
    syslink_uart_write(sl->fd, buffer, total_size);
    //pthread_mutex_unlock(&mutex);

    return 0;
}

int syslink_receive(struct _syslink *sl, syslink_packet_t *slp)
{
    static uint8_t buffer[64] = { 0 };
    int ret;

    while ((ret = syslink_uart_read(sl->fd, buffer, 64)) > 0) {

        for (int i = 0; i < ret; ++i) {
            cbuffer_push(&sl->cbuf, buffer[i]);
        }

        struct _syslink_packet *pkt;
        uint8_t c;

        while (!cbuffer_isempty(&sl->cbuf)) {

            cbuffer_pop(&sl->cbuf, &c);
            pkt = _parse(sl, c);
            if (pkt) {
                memcpy(slp, pkt, sizeof(syslink_packet_t));
                return 1;
            }
        }
    }

    /* /\* dump *\/ */
    /* printf("%s: (%d) ", __func__, ret); */
    /* for (int i=0; i < ret; ++i) { */
    /*     printf("%02x ", buffer[i]); */
    /* } */
    /* puts("\n"); */


    return 0;
}

int syslink_fd(syslink_t *sl)
{
    return sl ? sl->fd : -1;
}

void syslink_dump(syslink_packet_t *slp, const char *str)
{
    printf("%s (%s)\n  type: %s\n", __func__, str, syslink_type_name(slp->type));
    printf("  length: %d\n  data: ", slp->length);
    for (int i = 0; i < slp->length; ++i) {
        printf("%02x ", slp->data[i]);
    }
    puts("\n");
}

const char *syslink_type_name(uint8_t type)
{
	switch (type) {
	case SYSLINK_RADIO_RAW:
		return stringify(SYSLINK_RADIO_RAW);
	case SYSLINK_RADIO_CHANNEL:
		return stringify(SYSLINK_RADIO_CHANNEL);
	case SYSLINK_RADIO_DATARATE:
		return stringify(SYSLINK_RADIO_DATARATE);
	case SYSLINK_RADIO_CONTWAVE:
		return stringify(SYSLINK_RADIO_CONTWAVE);
	case SYSLINK_RADIO_RSSI:
		return stringify(SYSLINK_RADIO_RSSI);
	case SYSLINK_RADIO_ADDRESS:
		return stringify(SYSLINK_RADIO_ADDRESS);
	case SYSLINK_RADIO_RAW_BROADCAST:
		return stringify(SYSLINK_RADIO_RAW_BROADCAST);
	case SYSLINK_RADIO_POWER:
		return stringify(SYSLINK_RADIO_POWER);
	case SYSLINK_PM_SOURCE:
		return stringify(SYSLINK_PM_SOURCE);
	case SYSLINK_PM_ONOFF_SWITCHOFF:
		return stringify(SYSLINK_PM_ONOFF_SWITCHOFF);
	case SYSLINK_PM_BATTERY_VOLTAGE:
		return stringify(SYSLINK_PM_BATTERY_VOLTAGE);
	case SYSLINK_PM_BATTERY_STATE:
		return stringify(SYSLINK_PM_BATTERY_STATE);
	case SYSLINK_PM_BATTERY_AUTOUPDATE:
		return stringify(SYSLINK_PM_BATTERY_AUTOUPDATE);
	case SYSLINK_OW_SCAN:
		return stringify(SYSLINK_OW_SCAN);
	case SYSLINK_OW_GETINFO:
		return stringify(SYSLINK_OW_GETINFO);
	case SYSLINK_OW_READ:
		return stringify(SYSLINK_OW_READ);
	case SYSLINK_OW_WRITE:
		return stringify(SYSLINK_OW_WRITE);
	}
	return "";
}

