#include <stdint.h>
#include <string.h>

#include "crtp.h"
#include "syslink.h"
#include "shift_event.h"

struct _shift_event_arg {
	shift_event_t type;
};

static  void _make_event(struct _crtp *self, void *arg);

static struct _crtp crtp_shift_event = {
	.make_data = _make_event,
};

static  void _make_event(struct _crtp *self, void *arg)
{
    struct _shift_event_arg *ca = arg;

    if (SEVT_IS_CMD_EVENT(ca->type)) {
        *((uint16_t *)&self->data[0]) = ca->type;
        self->data[2] = 1;
        self->data_size = 3;
        return;

    } else if (SEVT_IS_RET_EVENT(ca->type)) {
        *((uint16_t *)&self->data[0]) = ca->type;
        self->data[2] = 1;
        self->data_size = 3;
        return;
    }
	
}

void shift_event_init()
{
	crtp_init(&crtp_shift_event);
}

void shift_event_deinit()
{
	crtp_deinit(&crtp_shift_event);
}

void shift_event_command(syslink_t *sl, unsigned int desPort, shift_event_t type)
{
	unsigned char buffer[24] = { 0 };
	int buffer_size = 0;
	struct _shift_event_arg arg = {
		.type = type
	};
	
	crtp_set_header(&crtp_shift_event, desPort, 1);
	crtp_pack(&crtp_shift_event, &arg, buffer, &buffer_size);

	syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}

void shift_event_result(syslink_t *sl, unsigned int desPort, shift_event_t type)
{
	unsigned char buffer[24] = { 0 };
	int buffer_size = 0;
	struct _shift_event_arg arg = {
		.type = type
	};

	crtp_set_header(&crtp_shift_event, desPort, 0);
	crtp_pack(&crtp_shift_event, &arg, buffer, &buffer_size);

	syslink_send(sl, SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
}
//
//void commander_velocity(float vx, float vy, float vz, float yawrate)
//{
//	uint8_t buffer[24] = { 0 };
//	int buffer_size = 0;
//	struct _commander_arg arg = {
//		.type = COMMANDER_TYPE_GENERIC,
//		.generic = {
//		.type = COMMANDER_GENERIC_TYPE_VELOCITYWORLD,
//		.velocity = {
//		.vx = vx,
//		.vy = vy,
//		.vz = vz,
//		.yawrate = yawrate,
//	}
//	}
//	};
//
//	crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
//	crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);
//
//	syslink_send(SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
//}
//
//void commander_zdistance(float roll, float pitch, float yawrate, float zdistance)
//{
//	uint8_t buffer[24] = { 0 };
//	int buffer_size = 0;
//	struct _commander_arg arg = {
//		.type = COMMANDER_TYPE_GENERIC,
//		.generic = {
//		.type = COMMANDER_GENERIC_TYPE_ZDISTANCE,
//		.zdistance = {
//		.roll = roll,
//		.pitch = pitch,
//		.yawrate = yawrate,
//		.zdistance = zdistance,
//	}
//	}
//	};
//
//	crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
//	crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);
//
//	syslink_send(SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
//}
//
//
//void commander_althold(float roll, float pitch, float yawrate, float zvelocity)
//{
//	uint8_t buffer[24] = { 0 };
//	int buffer_size = 0;
//	struct _commander_arg arg = {
//		.type = COMMANDER_TYPE_GENERIC,
//		.generic = {
//		.type = COMMANDER_GENERIC_TYPE_ALTHOLD,
//		.althold = {
//		.roll = roll,
//		.pitch = pitch,
//		.yawrate = yawrate,
//		.zvelocity = zvelocity,
//	}
//	}
//	};
//
//	crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
//	crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);
//
//	syslink_send(SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
//}
//
//void commander_hover(float vx, float vy, float yawrate, float zdistance)
//{
//	uint8_t buffer[24] = { 0 };
//	int buffer_size = 0;
//	struct _commander_arg arg = {
//		.type = COMMANDER_TYPE_GENERIC,
//		.generic = {
//		.type = COMMANDER_GENERIC_TYPE_HOVER,
//		.hover = {
//		.vx = vx,
//		.vy = vy,
//		.yawrate = yawrate,
//		.zdistance = zdistance,
//	}
//	}
//	};
//
//	crtp_set_header(&crtp_commander, CRTP_PORT_SETPOINT_GENERIC, 0);
//	crtp_pack(&crtp_commander, &arg, buffer, &buffer_size);
//
//	syslink_send(SYSLINK_RADIO_RAW, buffer + 1, buffer_size - 1);
//}
