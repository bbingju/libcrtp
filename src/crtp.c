#include <stdint.h>
#include "crtp.h"

crtp_ret_t crtp_init(crtp_t *c)
{
    return CRTP_RET_SUCCESS;
}

crtp_ret_t crtp_deinit(crtp_t *c)
{
    return CRTP_RET_SUCCESS;
}

crtp_ret_t crtp_set_header(crtp_t *c, crtp_port_t port, crtp_channel_t channel)
{
    c->port = port;
    c->channel = channel;

    return CRTP_RET_SUCCESS;
}

crtp_ret_t crtp_pack(crtp_t *c, void *arg, void *outbuf, int *size)
{
    uint8_t *offset;

    c->make_data(c, arg);

    offset = outbuf;

    /* size */
    *(offset++) = 1 + c->data_size;

    /* header */
    *(offset++) = ((c->port & 0x0F) << 4) | 3 << 2 | (c->channel & 0x03);

    /* data */
    int count = c->data_size;
    uint8_t *src = c->data;
    while (count--)
        *(offset++) = *(src++);

    *size = c->data_size + 2;

    return CRTP_RET_SUCCESS;
}

crtp_ret_t crtp_unpack(void *inbuf, crtp_t *c)
{
    uint8_t size, header;
    uint8_t *offset = inbuf;
    int data_size;

    size = *(offset++);
    header = *(offset++);
    data_size = size - 1;

    c->port = (header & 0xf0) >> 4;
    c->channel = header & 0x03;

    c->data_size = data_size > CRTP_MAX_DATA_SIZE ? CRTP_MAX_DATA_SIZE : data_size;
    for (int i = 0; i < c->data_size; ++i)
        c->data[i] = *(offset + i);

    return CRTP_RET_SUCCESS;
}
