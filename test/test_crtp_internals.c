#include "crtp_internals.h"
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP(crtp_internals);

struct _crtp_internals _crtp;
uint8_t test_data[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
uint8_t actual_data[30] = { 0 };

TEST_SETUP(crtp_internals)
{
    crtp_internals_init(&_crtp, 3, 4);
    _crtp.setdata(&_crtp, test_data, sizeof (test_data));
    _crtp.pack(&_crtp, actual_data);
}

TEST_TEAR_DOWN(crtp_internals)
{
}

TEST(crtp_internals, crtp_pack_ShouldCopiedDataIsSame)
{
    TEST_ASSERT_EQUAL_HEX8(sizeof(test_data) + 1, actual_data[0]);
    TEST_ASSERT_EQUAL_MEMORY(test_data, &actual_data[2], sizeof(test_data));
}


TEST(crtp_internals, crtp_unpack_ShouldCopiedDataIsSame)
{
    void *unpacked;
    int unpacked_size;

    _crtp.resetdata(&_crtp);
    _crtp.unpack(&_crtp, actual_data);
    _crtp.getdata(&_crtp, &unpacked, &unpacked_size);

    TEST_ASSERT_EQUAL_MEMORY(test_data, unpacked, unpacked_size);
}
