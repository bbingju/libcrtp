#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(crtp_internals)
{
    RUN_TEST_CASE(crtp_internals, crtp_pack_ShouldCopiedDataIsSame);
    RUN_TEST_CASE(crtp_internals, crtp_unpack_ShouldCopiedDataIsSame);
}
